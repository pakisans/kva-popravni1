import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Clan } from "../model/Clan";

@Injectable({
  providedIn: 'root'
})
export class ClanService {

  constructor(private http: HttpClient) { }

  dobaviSve(): Observable<Clan[]> {
    return this.http.get<Clan[]>(`http://localhost:3004/clan`);
  }

  dobaviJednog(id: number): Observable<Clan> {
    return this.http.get<Clan>(`http://localhost:3004/clan/${id}`);
  }

  dodaj(clan: Clan): Observable<Clan> {
    return this.http.post<Clan>(`http://localhost:3004/clan`, clan);
  }

  izmeni(clan: Clan): Observable<Clan> {
    return this.http.put<Clan>(
      `http://localhost:3004/clan/${clan.id}`,
      clan
    );
  }

  obrisi(id: number): Observable<Clan> {
    return this.http.delete<Clan>(`http://localhost:3004/clan/${id}`);
  }

}
