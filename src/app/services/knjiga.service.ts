import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { Knjiga} from "../model/Knjiga";
@Injectable({
  providedIn: 'root'
})
export class KnjigaService {

  constructor(private http: HttpClient) { }

  dobaviSve(): Observable<Knjiga[]> {
    return this.http.get<Knjiga[]>(`http://localhost:3004/knjiga`);
  }

  dobaviJednog(id: number): Observable<Knjiga> {
    return this.http.get<Knjiga>(`http://localhost:3004/knjiga/${id}`);
  }
  dodaj(knjiga: Knjiga): Observable<Knjiga> {
    return this.http.post<Knjiga>(`http://localhost:3004/knjiga`, knjiga);
  }

  izmeni(knjiga: Knjiga): Observable<Knjiga> {
    return this.http.put<Knjiga>(`http://localhost:3004/knjiga/${knjiga.id}`, knjiga);
  }

  obrisi(id: number): Observable<Knjiga> {
    return this.http.delete<Knjiga>(`http://localhost:3004/knjiga/${id}`);
  }


}
