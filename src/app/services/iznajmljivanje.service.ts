import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { Iznajmljivanje } from "../model/Iznajmljivanje";

@Injectable({
  providedIn: 'root'
})
export class IznajmljivanjeService {

  constructor(private http: HttpClient) { }

  dobaviSve(): Observable<Iznajmljivanje[]> {
    return this.http.get<Iznajmljivanje[]>(`http://localhost:3004/iznajmljivanje`);
  }

  dobaviJednog(id: number): Observable<Iznajmljivanje> {
    return this.http.get<Iznajmljivanje>(
      `http://localhost:3004/iznajmljivanje/${id}`
    );
  }

  dodaj(iznajmljivanje: Iznajmljivanje): Observable<Iznajmljivanje> {
    return this.http.post<Iznajmljivanje>(
      `http://localhost:3004/iznajmljivanje`,
      iznajmljivanje
    );
  }

  izmeni(iznajmljivanje: Iznajmljivanje): Observable<Iznajmljivanje> {
    return this.http.put<Iznajmljivanje>(
      `http://localhost:3004/iznajmljivanje/${iznajmljivanje.id}`,
      iznajmljivanje
    );
  }

  obrisi(id: number): Observable<Iznajmljivanje> {
    return this.http.delete<Iznajmljivanje>(
      `http://localhost:3004/iznajmljivanje/${id}`
    );
  }

}
