import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Knjiga } from "src/app/model/Knjiga";
import { Clan } from "src/app/model/Clan";
import { Iznajmljivanje } from "src/app/model/Iznajmljivanje";
import { IznajmljivanjeService } from "src/app/services/iznajmljivanje.service";
import { KnjigaService } from "src/app/services/knjiga.service";
import { ClanService } from "src/app/services/clan.service";

@Component({
  selector: 'app-iznajmljivanje-izmeni',
  templateUrl: './iznajmljivanje-izmeni.component.html',
  styleUrls: ['./iznajmljivanje-izmeni.component.css']
})
export class IznajmljivanjeIzmeniComponent implements OnInit {

  constructor(
    private iznajmljivanjeService: IznajmljivanjeService,
    private route: ActivatedRoute,
    private router: Router,
    private knjigaService: KnjigaService,
    private clanService: ClanService
  ) { }
  
  knjige: Knjiga[];
  clanovi: Clan[];

  

  izabranaKnjigaId: number;
  izabranClanId: number;

  iznajmljivanje: Iznajmljivanje = {
    id: null,
    knjigaId: null,
    clanId: null,
    datumVracanja: null,
    datumIznajmljivanja: null,
  };

  ngOnInit(): void {
    this.iznajmljivanjeService
      .dobaviJednog(this.route.snapshot.params["id"])
      .subscribe((response) => (this.iznajmljivanje = response));
    this.knjigaService
      .dobaviSve()
      .subscribe((response) => (this.knjige = response));
    this.clanService
      .dobaviSve()
      .subscribe((response) => (this.clanovi = response));
  }

  izmeni() {
    this.iznajmljivanje.knjigaId = this.izabranaKnjigaId;
    this.iznajmljivanje.clanId = this.izabranClanId;
    this.iznajmljivanjeService.izmeni(this.iznajmljivanje).subscribe(() => {
      this.router.navigate(["/iznajmljivanje"]);
    });
  }

}
