import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IznajmljivanjeIzmeniComponent } from './iznajmljivanje-izmeni.component';

describe('IznajmljivanjeIzmeniComponent', () => {
  let component: IznajmljivanjeIzmeniComponent;
  let fixture: ComponentFixture<IznajmljivanjeIzmeniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IznajmljivanjeIzmeniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IznajmljivanjeIzmeniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
