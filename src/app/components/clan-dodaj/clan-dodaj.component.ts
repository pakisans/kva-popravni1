import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Clan } from "src/app/model/Clan";

@Component({
  selector: 'app-clan-dodaj',
  templateUrl: './clan-dodaj.component.html',
  styleUrls: ['./clan-dodaj.component.css']
})
export class ClanDodajComponent implements OnInit {
  @Output() onSubmit: EventEmitter<Clan> = new EventEmitter();

  novClan: Clan = {
    id: null,
    ime: "",
    prezime: "",
  };

  constructor() { }

  ngOnInit(): void {
  }

  prosledi() {
    this.onSubmit.emit(this.novClan);
  }

}
