import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClanDodajComponent } from './clan-dodaj.component';

describe('ClanDodajComponent', () => {
  let component: ClanDodajComponent;
  let fixture: ComponentFixture<ClanDodajComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClanDodajComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClanDodajComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
