import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Iznajmljivanje } from "src/app/model/Iznajmljivanje";
import { IznajmljivanjeService } from "src/app/services/iznajmljivanje.service";

@Component({
  selector: 'app-iznajmljivanje',
  templateUrl: './iznajmljivanje.component.html',
  styleUrls: ['./iznajmljivanje.component.css']
})
export class IznajmljivanjeComponent implements OnInit {

  constructor(
    private iznajmljivanjeService: IznajmljivanjeService,
    private route: ActivatedRoute,
    private router: Router
  ) { }


  iznajmljivanje: Iznajmljivanje = {
    id: null,
    knjigaId: null,
    clanId: null,
    datumIznajmljivanja: null,
    datumVracanja: null,
  };

  ngOnInit(): void {
    this.dobaviIznajmljivanje();
  }

  dobaviIznajmljivanje() {
    this.iznajmljivanjeService
      .dobaviJednog(this.route.snapshot.params["id"])
      .subscribe((response) => (this.iznajmljivanje = response));
  }

  otvoriIzmenu() {
    this.router.navigate(["/iznajmljivanje", this.iznajmljivanje.id, "izmeni"]);
  }

}
