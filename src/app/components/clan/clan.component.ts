import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Clan } from "src/app/model/Clan";
import { ClanService } from "src/app/services/clan.service";

@Component({
  selector: 'app-clan',
  templateUrl: './clan.component.html',
  styleUrls: ['./clan.component.css']
})
export class ClanComponent implements OnInit {

  constructor(
    private clanService: ClanService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  clan: Clan = {
    id: null,
    ime: "",
    prezime: "",
  };


  ngOnInit(): void {
    this.dobaviClana();
  }

  dobaviClana() {
    this.clanService
      .dobaviJednog(this.route.snapshot.params["id"])
      .subscribe((response) => {
        this.clan = response;
      });
  }

  otvoriIzmenu() {
    this.router.navigate(["/clan", this.clan.id, "izmeni"]);
  }

}
