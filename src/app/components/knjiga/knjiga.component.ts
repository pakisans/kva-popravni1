import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Knjiga } from "src/app/model/Knjiga";
import { KnjigaService } from "src/app/services/knjiga.service";

@Component({
  selector: 'app-knjiga',
  templateUrl: './knjiga.component.html',
  styleUrls: ['./knjiga.component.css']
})
export class KnjigaComponent implements OnInit {

  constructor(
    private knjigaService: KnjigaService,
    private route: ActivatedRoute,
    private router: Router) { }
  
  
  knjiga: Knjiga = {
    id: null,
    naslov: "",
    autor: "",
    isbn: null,
  };

  ngOnInit(): void {
    this.dobaviKnjigu();
  }

  dobaviKnjigu() {
    this.knjigaService
      .dobaviJednog(this.route.snapshot.params["id"])
      .subscribe((response) => (this.knjiga = response));
  }
  otvoriIzmenu() {
    this.router.navigate(["/knjiga", this.knjiga.id, "izmeni"]);
  }

}
