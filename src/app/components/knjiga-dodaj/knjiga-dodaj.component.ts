import { Component,EventEmitter, OnInit, Output } from '@angular/core';
import { Knjiga } from "src/app/model/Knjiga";
@Component({
  selector: 'app-knjiga-dodaj',
  templateUrl: './knjiga-dodaj.component.html',
  styleUrls: ['./knjiga-dodaj.component.css']
})
export class KnjigaDodajComponent implements OnInit {
  @Output()
  onSubmit: EventEmitter<Knjiga> = new EventEmitter();

  novaKnjiga: Knjiga = {
    id: null,
    naslov: "",
    autor: "",
    isbn: null,
  };

  constructor() { }

  ngOnInit(): void {
  }

  prosledi() {
    this.onSubmit.emit(this.novaKnjiga);
  }

}
