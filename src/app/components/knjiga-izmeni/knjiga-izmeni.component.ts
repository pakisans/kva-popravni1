import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Knjiga } from "src/app/model/Knjiga";
import { KnjigaService } from "src/app/services/knjiga.service";

@Component({
  selector: 'app-knjiga-izmeni',
  templateUrl: './knjiga-izmeni.component.html',
  styleUrls: ['./knjiga-izmeni.component.css']
})
export class KnjigaIzmeniComponent implements OnInit {

  constructor(
    private knjigaService: KnjigaService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  knjiga: Knjiga = {
    id: null,
    naslov: "",
    autor: "",
    isbn: null,
  };

  ngOnInit(): void {
    this.knjigaService
      .dobaviJednog(this.route.snapshot.params["id"])
      .subscribe((response) => (this.knjiga = response));
  }
  izmeni() {
    this.knjigaService.izmeni(this.knjiga).subscribe(() => {
      this.router.navigate(["/knjiga"]);
    });
  }

}
