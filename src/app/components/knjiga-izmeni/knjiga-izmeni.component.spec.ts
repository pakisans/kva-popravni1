import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KnjigaIzmeniComponent } from './knjiga-izmeni.component';

describe('KnjigaIzmeniComponent', () => {
  let component: KnjigaIzmeniComponent;
  let fixture: ComponentFixture<KnjigaIzmeniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KnjigaIzmeniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KnjigaIzmeniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
