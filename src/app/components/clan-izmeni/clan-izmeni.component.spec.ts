import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClanIzmeniComponent } from './clan-izmeni.component';

describe('ClanIzmeniComponent', () => {
  let component: ClanIzmeniComponent;
  let fixture: ComponentFixture<ClanIzmeniComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClanIzmeniComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClanIzmeniComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
