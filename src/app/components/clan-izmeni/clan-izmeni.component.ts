import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Clan } from "src/app/model/Clan";
import { ClanService } from "src/app/services/clan.service";


@Component({
  selector: 'app-clan-izmeni',
  templateUrl: './clan-izmeni.component.html',
  styleUrls: ['./clan-izmeni.component.css']
})
export class ClanIzmeniComponent implements OnInit {

  constructor(
    private clanService: ClanService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  clan: Clan = {
    id: null,
    ime: "",
    prezime: "",
  };

  ngOnInit(): void {
    this.clanService
      .dobaviJednog(this.route.snapshot.params["id"])
      .subscribe((response) => (this.clan = response));
  }

  izmeni() {
    this.clanService.izmeni(this.clan).subscribe(() => {
      this.router.navigate(["/clan"]);
    });
  }

}
