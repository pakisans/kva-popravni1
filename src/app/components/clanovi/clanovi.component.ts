import { Component, OnInit } from '@angular/core';
import { Clan } from "src/app/model/Clan";
import { ClanService } from "src/app/services/clan.service";

@Component({
  selector: 'app-clanovi',
  templateUrl: './clanovi.component.html',
  styleUrls: ['./clanovi.component.css']
})
export class ClanoviComponent implements OnInit {

  constructor(private clanoviService: ClanService) { }

  clanovi: Clan[];

  ngOnInit(): void {
    this.dobaviClana();
  }

  dobaviClana() {
    this.clanoviService
      .dobaviSve()
      .subscribe((response) => (this.clanovi = response));
  }

  obrisi(id: number) {
    this.clanoviService.obrisi(id).subscribe(() => {
      this.dobaviClana();
    });
  }

  dodajClana(novClan: Clan) {
    this.clanoviService.dodaj(novClan).subscribe((response) => {
      this.dobaviClana();
    });
  }

}
