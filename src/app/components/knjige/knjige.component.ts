import { Component, OnInit } from '@angular/core';
import { Knjiga } from "src/app/model/Knjiga";
import { KnjigaService } from "src/app/services/knjiga.service";
@Component({
  selector: 'app-knjige',
  templateUrl: './knjige.component.html',
  styleUrls: ['./knjige.component.css']
})
export class KnjigeComponent implements OnInit {

  constructor(private knjigaService: KnjigaService) { }

  knjige: Knjiga[];

  ngOnInit(): void {
    this.dobaviKnjige();
  }

  dobaviKnjige() {
    this.knjigaService
      .dobaviSve()
      .subscribe((response) => (this.knjige = response));
  }

  obrisi(id: number) {
    this.knjigaService.obrisi(id).subscribe(() => {
      this.dobaviKnjige();
    });
  }

  dodajKnjigu(novaKnjiga) {
    this.knjigaService.dodaj(novaKnjiga).subscribe((response) => {
      this.dobaviKnjige();
    });
  }

}
