import { Component, OnInit } from '@angular/core';
import { Iznajmljivanje } from "src/app/model/Iznajmljivanje";
import { IznajmljivanjeService } from "src/app/services/iznajmljivanje.service";

@Component({
  selector: 'app-iznajmljivanja',
  templateUrl: './iznajmljivanja.component.html',
  styleUrls: ['./iznajmljivanja.component.css']
})
export class IznajmljivanjaComponent implements OnInit {

  constructor(private iznajmljivanjeService: IznajmljivanjeService) { }

  iznajmljivanja: Iznajmljivanje[];

  ngOnInit(): void {
    this.dobaviIznajmljivanja();
  }

  dobaviIznajmljivanja() {
    this.iznajmljivanjeService
      .dobaviSve()
      .subscribe((response) => (this.iznajmljivanja = response));
  }

  obrisi(id: number) {
    this.iznajmljivanjeService.obrisi(id).subscribe(() => {
      this.dobaviIznajmljivanja();
    });
  }
  dodajIznajmljivanje(novoIznajmljivanje) {
    this.iznajmljivanjeService.dodaj(novoIznajmljivanje).subscribe(() => {
      this.dobaviIznajmljivanja();
    });
  }

}
