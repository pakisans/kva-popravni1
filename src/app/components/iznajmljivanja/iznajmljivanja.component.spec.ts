import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IznajmljivanjaComponent } from './iznajmljivanja.component';

describe('IznajmljivanjaComponent', () => {
  let component: IznajmljivanjaComponent;
  let fixture: ComponentFixture<IznajmljivanjaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IznajmljivanjaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IznajmljivanjaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
