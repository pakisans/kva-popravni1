import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Knjiga } from "src/app/model/Knjiga";
import { Iznajmljivanje } from "src/app/model/Iznajmljivanje";
import { Clan } from "src/app/model/Clan";
import { KnjigaService } from "src/app/services/knjiga.service";
import { ClanService } from "src/app/services/clan.service";
@Component({
  selector: 'app-iznajmljivanje-dodaj',
  templateUrl: './iznajmljivanje-dodaj.component.html',
  styleUrls: ['./iznajmljivanje-dodaj.component.css']
})
export class IznajmljivanjeDodajComponent implements OnInit {
  @Output() onSubmit: EventEmitter<Iznajmljivanje> = new EventEmitter();

  
  novoIznajmljivanje: Iznajmljivanje = {
    id: null,
    knjigaId: null,
    clanId: null,
    datumVracanja: null,
    datumIznajmljivanja: null,
  };

  knjige: Knjiga[];
  clanovi: Clan[];

  izabranaKnjigaId: number = null;
  izabranClanId: number = null;
  
  constructor(
    private knjigaService: KnjigaService,
    private clanService: ClanService
  ) { }

  ngOnInit(): void {
    this.knjigaService.dobaviSve().subscribe((response) => {
      this.knjige = response;
    });
    this.clanService.dobaviSve().subscribe((response) => {
      this.clanovi = response;
    });
  }

  prosledi() {
    this.novoIznajmljivanje.knjigaId = this.izabranaKnjigaId;
    this.novoIznajmljivanje.clanId = this.izabranClanId;
    this.onSubmit.emit(this.novoIznajmljivanje);
  }

}
