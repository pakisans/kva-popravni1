import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IznajmljivanjeDodajComponent } from './iznajmljivanje-dodaj.component';

describe('IznajmljivanjeDodajComponent', () => {
  let component: IznajmljivanjeDodajComponent;
  let fixture: ComponentFixture<IznajmljivanjeDodajComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IznajmljivanjeDodajComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IznajmljivanjeDodajComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
