import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClanIzmeniComponent } from "./components/clan-izmeni/clan-izmeni.component";
import { IznajmljivanjeIzmeniComponent } from "./components/iznajmljivanje-izmeni/iznajmljivanje-izmeni.component";
import { IznajmljivanjeComponent } from "./components/iznajmljivanje/iznajmljivanje.component";
import { ClanComponent } from "./components/clan/clan.component";
import { KnjigaIzmeniComponent } from "./components/knjiga-izmeni/knjiga-izmeni.component";
import { KnjigaComponent } from "./components/knjiga/knjiga.component";
import { KnjigeComponent } from "./components/knjige/knjige.component";
import { IznajmljivanjaComponent } from './components/iznajmljivanja/iznajmljivanja.component';
import { ClanoviComponent } from './components/clanovi/clanovi.component';



const routes: Routes = [
  {
    path: "knjiga",
    component: KnjigeComponent
  },
  {
    path:"knjiga/:id",
    component: KnjigaComponent
  },
  {
    path:"knjiga/:id/izmeni",
    component: KnjigaIzmeniComponent
  },
  {
    path:"iznajmljivanje",
    component: IznajmljivanjaComponent,
  },
  {
    path: "iznajmljivanje/:id",
    component: IznajmljivanjeComponent
  },
  {
    path:"iznajmljivanje/:id/izmeni",
    component: IznajmljivanjeIzmeniComponent,
  },
  {
    path: "clan",
    component: ClanoviComponent,
  },
  {
    path: "clan/:id",
    component : ClanComponent,
  },
  {
    path:"clan/:id/izmeni",
    component: ClanIzmeniComponent,
  },
  {
    path:"",
    component: KnjigeComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
