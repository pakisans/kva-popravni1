import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { KnjigaComponent } from './components/knjiga/knjiga.component';
import { KnjigeComponent } from './components/knjige/knjige.component';
import { KnjigaDodajComponent } from './components/knjiga-dodaj/knjiga-dodaj.component';
import { KnjigaIzmeniComponent } from './components/knjiga-izmeni/knjiga-izmeni.component';
import { ClanComponent } from './components/clan/clan.component';
import { ClanoviComponent } from './components/clanovi/clanovi.component';
import { ClanDodajComponent } from './components/clan-dodaj/clan-dodaj.component';
import { ClanIzmeniComponent } from './components/clan-izmeni/clan-izmeni.component';
import { IznajmljivanjeComponent } from './components/iznajmljivanje/iznajmljivanje.component';
import { IznajmljivanjaComponent } from './components/iznajmljivanja/iznajmljivanja.component';
import { IznajmljivanjeDodajComponent } from './components/iznajmljivanje-dodaj/iznajmljivanje-dodaj.component';
import { IznajmljivanjeIzmeniComponent } from './components/iznajmljivanje-izmeni/iznajmljivanje-izmeni.component';

@NgModule({
  declarations: [
    AppComponent,
    KnjigaComponent,
    KnjigeComponent,
    KnjigaDodajComponent,
    KnjigaIzmeniComponent,
    ClanComponent,
    ClanoviComponent,
    ClanDodajComponent,
    ClanIzmeniComponent,
    IznajmljivanjeComponent,
    IznajmljivanjaComponent,
    IznajmljivanjeDodajComponent,
    IznajmljivanjeIzmeniComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
